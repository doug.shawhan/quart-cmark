"""
Basic tests of inline and block style rendering.
"""

import pytest
import pytest_asyncio

from quart import Quart, render_template_string

from quart_cmark import Cmark

# pylint: disable=redefined-outer-name


class QuartApp:  # pylint: disable=too-few-public-methods
    """
    Fakey quart app
    """

    def __init__(self, auto_escape):
        """
        Set auto_escape here.
        """
        self.auto_escape = auto_escape

    def get_app(self):
        """
        Generate the app for tests to use.
        """
        app = Quart(__name__)
        app.debug = True
        Cmark(app, auto_escape=self.auto_escape)

        @app.route("/test_inline")
        async def view_render_inline():
            """
            inline route
            """
            mycm = "Hello, *commonmark*."
            return await render_template_string("{{mycm|commonmark}}", mycm=mycm,)

        @app.route("/test_var_block")
        async def view_render_var_block():
            """
            var block
            """
            mycm = "Hello, *commonmark* block."
            template = """\
{% filter commonmark %}
{{mycm}}
{% endfilter %}"""
            return await render_template_string(template, mycm=mycm)

        @app.route("/test_in_block")
        async def view_render_in_block():
            """
            Corner case where user wants to translate static markdown.
            """
            template = """\
{% filter commonmark %}
Hello, *commonmark* block.
{% endfilter %}"""
            return await render_template_string(template)

        return app


@pytest_asyncio.fixture
def quart_app(request):
    """
    Create a fixture
    """
    return QuartApp(request.param)


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_inline(quart_app):
    """
    Render simple inline
    """
    response = await quart_app.get_app().test_client().get("/test_inline")
    assert await response.data == b"<p>Hello, <em>commonmark</em>.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_var_block(quart_app):
    """
    Render useful var block
    """
    response = await quart_app.get_app().test_client().open("/test_var_block")
    assert await response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_in_block(quart_app):
    """
    Test dubious static data.
    """
    response = await quart_app.get_app().test_client().open("/test_in_block")
    assert await response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (True,), indirect=True)
async def test_render_autoescape_template_switch_off(quart_app):
    """
    Auto escape true
    """
    response = await quart_app.get_app().test_client().open("/test_var_block")
    assert (
        await response.data
        == b"&lt;p&gt;Hello, &lt;em&gt;commonmark&lt;/em&gt; block.&lt;/p&gt;\n"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (True,), indirect=True)
async def test_render_autoescape_template_switch_on(quart_app):
    """
    Auto escape true
    """
    response = await quart_app.get_app().test_client().open("/test_inline")
    assert (
        await response.data
        == b"&lt;p&gt;Hello, &lt;em&gt;commonmark&lt;/em&gt;.&lt;/p&gt;\n"
    )
