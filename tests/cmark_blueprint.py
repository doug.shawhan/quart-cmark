"""
Gin up a blueprint, mainly used to test factory pattern handling.
"""

from quart import Blueprint, render_template_string
from quart_cmark import Cmark

blueprint = Blueprint("commonmark", __name__)
commonmark = Cmark()


@blueprint.route("/test_inline")
async def view_render_inline():
    """
    Simple inline processing test.
    """
    mycm = "Hello, *commonmark*."
    return await render_template_string("{{mycm|commonmark}}", mycm=mycm)


@blueprint.route("/test_var_block")
async def view_render_var_block():
    """
    Block test with filter
    """
    mycm = "Hello, *commonmark* block."
    template = """\
{% filter commonmark %}
{{mycm}}
{% endfilter %}"""
    return await render_template_string(template, mycm=mycm)


@blueprint.route("/test_in_block")
async def view_render_in_block():
    """
    Filter markdown in "page". Not real useful.
    """
    template = """\
{% filter commonmark %}
Hello, *commonmark* block.
{% endfilter %}"""
    return await render_template_string(template)
