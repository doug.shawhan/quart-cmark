"""
Test module with blueprints/factory pattern
"""
import pytest
import pytest_asyncio
from quart import Quart

from tests.cmark_blueprint import commonmark
from tests.cmark_blueprint import blueprint

# pylint: disable=redefined-outer-name

class QuartApp: # pylint: disable=too-few-public-methods
    """
    Quart app for testing
    """

    def __init__(self, auto_escape):
        """
        Set auto_escape
        """
        self.auto_escape = auto_escape

    def get_app(self):
        """
        import commonmark and blueprint from blueprint module, then
        create an app.

        Cmark() was imported and  initialized as ``commonmark`` in
        ``cmark_blueprint``.
        """
        app = Quart(__name__)
        app.debug = True

        commonmark.init_app(app, auto_escape=self.auto_escape)
        app.register_blueprint(blueprint)

        return app


@pytest_asyncio.fixture
def quart_app(request):
    """
    create a fixture
    """
    return QuartApp(request.param)


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_inline(quart_app):
    """
    Test inline render
    """
    response = await quart_app.get_app().test_client().get("/test_inline")
    assert await response.data == b"<p>Hello, <em>commonmark</em>.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_var_block(quart_app):
    """
    Render within a block
    """
    response = await quart_app.get_app().test_client().get("/test_var_block")
    assert await response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (False,), indirect=True)
async def test_render_in_block(quart_app):
    """
    Render within a block
    """
    response = await quart_app.get_app().test_client().get("/test_in_block")
    assert await response.data == b"<p>Hello, <em>commonmark</em> block.</p>\n"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", (True,), indirect=True)
async def test_render_autoescape(quart_app):
    """
    test inline with auto_escape
    """
    response = await quart_app.get_app().test_client().get("/test_inline")
    assert (
        await response.data
        == b"&lt;p&gt;Hello, &lt;em&gt;commonmark&lt;/em&gt;.&lt;/p&gt;\n"
    )
