.. quart_cmark documentation master file, created by
   sphinx-quickstart on Mon Jul 25 17:10:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

quart-cmark
===========

Add CommonMark processing filter to your `Quart` app. https://commonmark.org/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: quart_cmark
    :members:
    :private-members:
    :undoc-members:
    :show-inheritance:
    :imported-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
